#!/bin/bash

# Fetch WordPress
wget https://wordpress.org/latest.tar.gz
tar -xzf latest.tar.gz

# Prepare the DB (only for local DB, this is already done with RDS)
#mysql -f -u root -pchangeme <<DBSCRIPT
#CREATE USER 'wordpress-user'@'localhost' IDENTIFIED BY 'changeme';
#CREATE DATABASE wordpress;
#GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpress-user'@'localhost';
#FLUSH PRIVILEGES;
#DBSCRIPT

# Configure Wordpress
cp wordpress/wp-config-sample.php wordpress/wp-config.php
sed -i -e "s/define( 'DB_NAME', 'database_name_here' );/define( 'DB_NAME', 'wpdb' );/" wordpress/wp-config.php
sed -i -e "s/define( 'DB_USER', 'username_here' );/define( 'DB_USER', 'admin' );/" wordpress/wp-config.php
sed -i -e "s/define( 'DB_PASSWORD', 'password_here' );/define( 'DB_PASSWORD', 'changeme' );/" wordpress/wp-config.php
sed -i -e "s/localhost/dbinstance.coh19lckqzph.eu-west-1.rds.amazonaws.com/g" /wordpress/wp-config.php

# Change Apache directory owner and copy all Wordpress files to Apache root directory
usermod -a -G apache ec2-user
chown -R ec2-user:apache /var/www
chmod 2775 /var/www && find /var/www -type d -exec sudo chmod 2775 {} \;
find /var/www -type f -exec sudo chmod 0664 {} \;

cp -r wordpress/* /var/www/html/

systemctl restart httpd
